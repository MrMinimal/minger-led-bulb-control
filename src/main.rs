extern crate rumble;
extern crate rand;

use std::thread;
use std::time::Duration;
use rumble::bluez::manager::Manager;
use rumble::api::{Central, Peripheral};

#[macro_use] extern crate rocket;

#[get("/")]
fn index() -> &'static str {
    on();
    "Turned on!"
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![index])
}


fn off() {
    let manager = Manager::new().unwrap();
    // get the first bluetooth adapter
    let adapters = manager.adapters().unwrap();
    let mut adapter = adapters.into_iter().nth(0).unwrap();
    // reset the adapter -- clears out any errant state
    adapter = manager.down(&adapter).unwrap();
    adapter = manager.up(&adapter).unwrap();
    // connect to the adapter
    let central = adapter.connect().unwrap();
    // start scanning for devices
    central.start_scan().unwrap();
    // instead of waiting, you can use central.on_event to be notified of
    // new devices
    thread::sleep(Duration::from_secs(2));
    println!("List of availible devices:\n");
    // find the device we're interested in
    central.peripherals().into_iter()
        .for_each(|p|
            match p.properties().local_name {
                Some(x) => println!("{}", x),
                None => println!("{}", "NONE"),
            }
        );
    println!("");
    let light = central.peripherals().into_iter()
        .find(|p| p.properties().local_name.iter()
            .any(|name| name.contains("Minger_H6001_557B"))).unwrap();
    // connect to the device
    light.connect().unwrap();
    // discover characteristics
    light.discover_characteristics().unwrap();
    // find the characteristic we want
    let chars = light.characteristics();
    let cmd_char = chars.iter().find(|c| format!("{}", c.uuid) == "00:01:02:03:04:05:06:07:08:09:0A:0B:0C:0D:2B:11").unwrap();
    // for _ in 0..20 {
    //     let keepalive = vec![0xaa, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xab];
    //     light.command(&cmd_char, &keepalive).unwrap();
    //     thread::sleep(Duration::from_secs(1));
    // }
    let mut brightness: u8 = 255;
    loop {
        //let on_cmd = vec![0x33, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x33];
        //let on_cmd = vec![0x33, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x33];
        let weird_value = generate_weird_brightness_value(brightness);

        let brightness_cmd = vec![0x33, 0x04, brightness, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, weird_value];

        light.command(&cmd_char, &brightness_cmd).unwrap();
        thread::sleep(Duration::from_millis(70));

        if brightness > 10 {
            brightness -= 10;
        } else {
            brightness = 0;
        }

        if brightness <= 0 {
            let off_cmd = vec![0x33, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x32];
            light.command(&cmd_char, &off_cmd).unwrap();
            break;
        }
    }
}

fn on() {
    let manager = Manager::new().unwrap();
    // get the first bluetooth adapter
    let adapters = manager.adapters().unwrap();
    let mut adapter = adapters.into_iter().nth(0).unwrap();
    // reset the adapter -- clears out any errant state
    adapter = manager.down(&adapter).unwrap();
    adapter = manager.up(&adapter).unwrap();
    // connect to the adapter
    let central = adapter.connect().unwrap();
    // start scanning for devices
    central.start_scan().unwrap();
    // instead of waiting, you can use central.on_event to be notified of
    // new devices
    thread::sleep(Duration::from_secs(2));
    println!("List of availible devices:\n");
    // find the device we're interested in
    central.peripherals().into_iter()
        .for_each(|p|
            match p.properties().local_name {
                Some(x) => println!("{}", x),
                None => println!("{}", "NONE"),
            }
        );
    println!("");
    let light = central.peripherals().into_iter()
        .find(|p| p.properties().local_name.iter()
            .any(|name| name.contains("Minger_H6001_557B"))).unwrap();
    // connect to the device
    light.connect().unwrap();
    // discover characteristics
    light.discover_characteristics().unwrap();
    // find the characteristic we want
    let chars = light.characteristics();
    let cmd_char = chars.iter().find(|c| format!("{}", c.uuid) == "00:01:02:03:04:05:06:07:08:09:0A:0B:0C:0D:2B:11").unwrap();
    // for _ in 0..20 {
    //     let keepalive = vec![0xaa, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xab];
    //     light.command(&cmd_char, &keepalive).unwrap();
    //     thread::sleep(Duration::from_secs(1));
    // }
    let mut brightness: u8 = 0;
    loop {
        //let on_cmd = vec![0x33, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x33];
        //let on_cmd = vec![0x33, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x33];
        let weird_value = generate_weird_brightness_value(brightness);

        let brightness_cmd = vec![0x33, 0x04, brightness, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, weird_value];

        light.command(&cmd_char, &brightness_cmd).unwrap();
        thread::sleep(Duration::from_millis(70));

        if brightness == 255 {
            break;
        }

        brightness += 10;
        if brightness > 230 {
            brightness = 255;
        }
    }
}
// Generates a value for a brightness command, I have no idea what it does
// but it is generated this way:
// Expects x to be [0, 255] which corresponds to brightness
fn generate_weird_brightness_value(x: u8) -> u8 {
    let mut result: i64 = 55 - ((x % 64) as i64);

    let a = x/8;
    if (a % 2) != 0 {
        result += 16;
    }

    let b = x/64;
    result += (b * 64) as i64;

    return result as u8
}
